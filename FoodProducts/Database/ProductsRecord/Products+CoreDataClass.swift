//
//  Products+CoreDataClass.swift
//  
//
//  Created by Sheetal Jha on 18/02/22.
//
//

import Foundation
import CoreData

public class ProductsRecord: NSManagedObject {
    
}

extension ProductsRecord {
    func getProductModel() -> Product {
        Product(name: self.name,
                price: self.price,
                image: self.image,
                pDescription: self.pDescription,
                id: self.id,
                categories: self.categories as? [String])
    }
}
