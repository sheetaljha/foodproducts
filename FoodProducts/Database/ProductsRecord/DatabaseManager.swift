//
//  DatabaseManager.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 21/02/22.
//

import Foundation
import CoreData

class DatabaseManager {
    
    let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
    
    func save(products: [Product]) {
        for product in products {
            guard let productsRecord = NSEntityDescription.insertNewObject(
                forEntityName: "ProductsRecord",
                into: context
            ) as? ProductsRecord else {
                continue
            }
            
            productsRecord.update(from: product)
        }
        
        do {
            try context.save()
        } catch let error {
            print(error)
        }
    }
    
    func fetchFromDatabase(completion: @escaping (Result<[ProductsRecord], Error>) -> Void) {
        // Create Fetch Request
        let fetchRequest = ProductsRecord.fetchRequest() as NSFetchRequest<ProductsRecord>
        // Perform Fetch Request
        do {
            // Execute Fetch Request
            let result = try context.fetch(fetchRequest)
            
            // Update Products Label
            completion(.success(result))
            
        } catch {
            print("Unable to Execute Fetch Request, \(error)")
            completion(.failure(error))
        }
    }
    
}

extension ProductsRecord {
    
    func update(from product: Product) {
        id = product.id
        name = product.name
        image = product.image
        price = product.price
        categories = product.categories as NSObject?
        pDescription = product.pDescription
    }
    
}
