//
//  Products+CoreDataProperties.swift
//  
//
//  Created by Sheetal Jha on 18/02/22.
//
//

import Foundation
import CoreData


extension ProductsRecord {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProductsRecord> {
        return NSFetchRequest<ProductsRecord>(entityName: "ProductsRecord")
    }
    
    @NSManaged public var id: String?
    @NSManaged public var image: String?
    @NSManaged public var price: String?
    @NSManaged public var name: String?
    @NSManaged public var pDescription: String?
    @NSManaged public var categories: NSObject?
    
}
