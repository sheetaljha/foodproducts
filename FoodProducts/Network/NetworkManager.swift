//
//  NetworkManager.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 09/02/22.
//

import UIKit

class NetworkManager {
    
    func fetchProductListFromRemote(completion: @escaping (Result<[Product], Error>) -> Void) {
        
        guard let url = URL(string: Environment.development.baseURL + ApiEndPoints.productList.path) else {
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) -> Void in
            if let data = data,
               let products = try? JSONDecoder().decode([Product].self, from: data) {
                completion(.success(products))
            } else if let error = error {
                completion(.failure(error))
                print("HTTP Request Failed \(error)")
            }
        }.resume()
    }
    
}
