//
//  EndPoints.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 11/02/22.
//

import UIKit

enum Environment {
    case development
    case production
    case staging
    case uat
}

extension Environment {
    static var current: Environment = .development
}

extension Environment {
    
    var baseURL: String {
        let baseUrl: String
        switch self {
        case .development:
            baseUrl = "https://618b78e1ded7fb0017bb8fbb.mockapi.io/api/v1"
        default:
            // for time being
            baseUrl = "https://618b78e1ded7fb0017bb8fbb.mockapi.io/api/v1"
        }
        return baseUrl
    }
    
}

enum ApiEndPoints {
    case productList
    case productDetails(id: String)
}

extension ApiEndPoints {
    
    var path: String {
        let path: String
        switch self {
        case .productList:
            path = "products"
        case .productDetails(let id):
            path = "products/\(id)"
        }
        return path
    }
    
}
