//
//  ProductTableViewCell.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 12/02/22.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    static let reusableIndentifier = "reuseCell"
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var categoriesLabel: UILabel!
    @IBOutlet private weak var productSmallImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(with product: Product) {
        nameLabel.text = product.name
        priceLabel.text = product.price
        categoriesLabel.text = product.categories?.joined(separator: ", ")
        
        let imageString = product.image ?? ""
        let imageURL = URL(string: imageString)
        productSmallImage.sd_setImage(with: imageURL,
                                      placeholderImage: UIImage(named: "placeholder.png"))
    }
    
}
