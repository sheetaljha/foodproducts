//
//  ProductListViewController.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 08/02/22.
//

import UIKit
import SDWebImage

final class ProductListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var loader: UIActivityIndicatorView!
    
    private let viewModel = ProductListViewModel()
    private let databaseManager = DatabaseManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.output = self
        setUpTableView()
        loader.hidesWhenStopped = true
        loader.startAnimating()
        self.loader.hidesWhenStopped = true
    }
    
    private func setUpTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 80
        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: ProductTableViewCell.reusableIndentifier)
        viewModel.refreshProductList()
    }
}

extension ProductListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let productDetailViewController = ProductDetailViewController(model: viewModel.getProduct(at: indexPath))
        navigationController?.pushViewController(productDetailViewController, animated: true)
    }
}

extension ProductListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.productListCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.reusableIndentifier,
                                                       for: indexPath) as? ProductTableViewCell,
              let product = viewModel.getProduct(at: indexPath) else {
                  return UITableViewCell()
              }
        
        cell.configureCell(with: product)
        
        return cell
    }
}

extension ProductListViewController: ProductListViewModelDelegate {
    func productListFetchSuccess() {
        DispatchQueue.main.async { [self] in
            self.loader.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func productListFetchFailure() {
        print("Call failed")
    }
    
}
