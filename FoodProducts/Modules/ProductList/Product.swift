//
//  Product.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 09/02/22.
//

import UIKit

struct Product: Decodable {
    let name, price, image, pDescription, id: String?
    let categories: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name = "productName"
        case price = "productPrice"
        case image = "productImage"
        case id = "productId"
        case categories = "productCategories"
        case pDescription = "productDescription"
    }
    
}

