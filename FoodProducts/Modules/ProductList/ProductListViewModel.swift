//
//  ProductListViewModel.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 11/02/22.
//

import UIKit

protocol ProductListViewModelDelegate: AnyObject {
    func productListFetchSuccess()
    func productListFetchFailure()
}

extension ProductListViewModelDelegate {
    func productListFetchSuccess() {}
    func productListFetchFailure() {}
}

class ProductListViewModel {
    
    private let networkManager = NetworkManager()
    private var productList: [Product] = []
    private var databaseManager = DatabaseManager()
    
    weak var output: ProductListViewModelDelegate?
    
    func refreshProductList() {
        databaseManager.fetchFromDatabase {
            (result: Result<[ProductsRecord], Error>) in
            switch result {
            case .success(let list):
                if list.isEmpty {
                    self.callProductListAPI()
                } else {
                    self.productList = list.map { $0.getProductModel() }
                    self.output?.productListFetchSuccess()
                }
            case .failure(_):
                self.output?.productListFetchFailure()
            }
        }
    }
    
    func callProductListAPI() {
        networkManager.fetchProductListFromRemote { (result: Result<[Product], Error>) in
            switch result {
            case .success(let list):
                self.productList = list
                self.databaseManager.save(products: list)
                self.output?.productListFetchSuccess()
                
            case .failure(let error):
                self.output?.productListFetchFailure()
            }
        }
    }
    
    var productListCount: Int {
        productList.count
    }
    
    func getProduct(at indexPath: IndexPath) -> Product? {
        guard indexPath.row < productListCount else {
            return nil
        }
        return productList[indexPath.row]
    }
}
