//
//  ProductDetailViewController.swift
//  FoodProducts
//
//  Created by Sheetal Jha on 17/02/22.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var categoriesLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    private let model: Product?
    init(model: Product?) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView() {
        let imageString = model?.image ?? ""
        let imageURL = URL(string: imageString)
        productImage.sd_setImage(with: imageURL,
                                 placeholderImage: UIImage(named: "placeholder.png"))
        nameLabel.text = model?.name
        priceLabel.text = model?.price
        categoriesLabel.text = model?.categories?.joined(separator: ", ")
        descriptionLabel.text = model?.pDescription
    }
    
}
